export abstract class AppSettingsCidenet {
  static readonly PATH_API_BACKENDDUMMY = 'http://localhost:3000';
  static readonly EMPLOYEE_ENDPOINT = 'employees';
  static readonly TYPE_IDENTIFICATION_ENDPOINT = 'tipos_identificacion';
  static readonly COUNTRY_ENDPOINT = 'paises';
  static readonly AREA_ENDPOINT = 'areas';
  static readonly LONGITUD_EMAIL_MAX = 300;
  // Registros por página del listado principal
  static readonly LONGITUD_REGISTROS_POR_PAGE = 10;

  static readonly DOMINIO_EMAIL_CO = 'cidenet.com.co';
  static readonly DOMINIO_EMAIL_USA = 'cidenet.com.us';
}
