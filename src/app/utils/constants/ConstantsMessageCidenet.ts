export abstract class ConstantsMessageCidenet {
  private static readonly QUESTION_SAVE_EMPLOYEE = '¿Está seguro que desea guardar el empleado? SI / NO';
  private static readonly QUESTION_EDIT_EMPLOYEE = '¿Está seguro que desea actualizar la información del empleado? SI / NO';
  private static readonly QUESTION_DELETE_EMPLOYEE = '¿Está seguro que desea eliminar el empleado? SI / NO';
  // tslint:disable-next-line:max-line-length
  private static readonly WARNING_EMPLOYEE_EMAIL_EXISTS = 'El email que intenta actualizar ya se encuentra registrado en la plataforma, ingrese uno distinto que cumpla con los estándares de Cidenet';
  // tslint:disable-next-line:max-line-length
  private static readonly WARNING_EMPLOYEE_IDENTIFICATION_TYPE_EXISTS = 'El número de identificación y Tipo que intenta actualizar ya se encuentra registrado en la plataforma, ingrese uno distinto que cumpla con los estándares de Cidenet';
  private static readonly WARNING_EMPLOYEE_DATE_POST = 'La fecha de ingreso NO puede ser posterior a la fecha actual.';
  // tslint:disable-next-line:max-line-length
  private static readonly WARNING_EMPLOYEE_DATE_INF_MONTH = 'La fecha de ingreso NO puede ser de más de 1 mes a la fecha actual, mínimo son 30 días antes.';

  static getArrMessageConstants() {
    return [{
      'message.employee.save': this.QUESTION_SAVE_EMPLOYEE,
      'message.employee.edit': this.QUESTION_EDIT_EMPLOYEE,
      'message.employee.delete': this.QUESTION_DELETE_EMPLOYEE,
      'message.employee.warning.email': this.WARNING_EMPLOYEE_EMAIL_EXISTS,
      'message.employee.warning.identification': this.WARNING_EMPLOYEE_IDENTIFICATION_TYPE_EXISTS,
      'message.employee.warning.date.post': this.WARNING_EMPLOYEE_DATE_POST,
      'message.employee.warning.date.inf.month': this.WARNING_EMPLOYEE_DATE_INF_MONTH
    }];
  }
}
