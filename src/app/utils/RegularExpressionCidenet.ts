export abstract class RegularExpressionCidenet {
  static readonly AZaz = /^[a-zA-Z]+$/;
  static readonly AZazSpace = /^[a-zA-Z ]+$/;
  static readonly AZaz09Guion = /^[a-zA-Z ]+$/;
  // tslint:disable-next-line:max-line-length
  static readonly emailValid = /^[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?$/;
}
