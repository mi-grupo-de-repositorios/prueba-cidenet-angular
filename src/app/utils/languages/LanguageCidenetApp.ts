export class LanguageCidenetApp {
  public static spanishDatatables = {
    processing: 'Procesando...',
    search: 'Buscar por (Nombre, Apellidos, Identificación, Email, País de empleo, Estado):',
    lengthMenu: 'Mostrar _MENU_ elementos',
    info: 'Mostrando desde _START_ al _END_ de _TOTAL_ elementos',
    infoEmpty: 'Mostrando ningún elemento.',
    infoFiltered: '(filtrado _MAX_ elementos total)',
    infoPostFix: '',
    loadingRecords: 'Cargando registros...',
    zeroRecords: 'No se encontraron registros',
    emptyTable: 'No hay datos disponibles en la tabla',
    paginate: {
      first: 'Primero',
      previous: 'Anterior',
      next: 'Siguiente',
      last: 'Último'
    },
    aria: {
      sortAscending: ': Activar para ordenar la tabla en orden ascendente',
      sortDescending: ': Activar para ordenar la tabla en orden descendente'
    }
  };
}
