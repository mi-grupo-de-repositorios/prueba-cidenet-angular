import { Component, OnInit } from '@angular/core';
import { EmployeeService } from '../../../services/employee/employee.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { DatePipe } from '@angular/common';
import { Router, ActivatedRoute } from '@angular/router';
import { RegularExpressionCidenet } from '../../../utils/RegularExpressionCidenet';
import { Employee } from '../../../models-view/employee.model';
import { ConstantsMessageCidenet } from '../../../utils/constants/ConstantsMessageCidenet';
import { ValidationsService } from '../../../services/employee/validations.service';

@Component({
  selector: 'app-edit-employee',
  templateUrl: './edit-employee.component.html',
  styleUrls: ['./edit-employee.component.scss']
})
export class EditEmployeeComponent implements OnInit {

  // miembros base
  editEmployeeForm: FormGroup;
  listTypesIdentification: any = [];
  listCountries: any = [];
  listAreas: any = [];
  dataEmployee: Employee;
  idEmployeEdit: number;

  displayStyleDialog = 'none';
  actionForm = 'Actualización';
  fechaHora: string;

  messagesView: any = {};

  constructor(
    private employeeService: EmployeeService,
    private validationService: ValidationsService,
    public formBuilder: FormBuilder,
    private datePipe: DatePipe,
    private router: Router,
    private activateRoute: ActivatedRoute
  ) {
    this.buildForm();

    // cargar tipos de identificación
    employeeService.getAllTypesIdentification().subscribe((response) => {
      this.listTypesIdentification = response;
    },
    error => {
      console.log(error);
      alert('EXCEPCIÓN CONTROLADA: ' + error.message);
    });

    // cargar Paises
    employeeService.getAllCountries().subscribe((response) => {
      this.listCountries = response;
    },
    error => {
      console.log(error);
      alert('EXCEPCIÓN CONTROLADA: ' + error.message);
    });

    // cargar Areas
    employeeService.getAllAreas().subscribe((response) => {
      this.listAreas = response;
    },
    error => {
      console.log(error);
      alert('EXCEPCIÓN CONTROLADA: ' + error.message);
    });

    // cargar constantes para componentes
    this.messagesView = ConstantsMessageCidenet.getArrMessageConstants()[0];
  }

  private buildForm(data?: Employee) {
    const datet =  new Date();

    this.editEmployeeForm = this.formBuilder.group({
      id: [data ? data.id : ''],
      primerNombre: [data ? data.primerNombre : '', [
        Validators.pattern(RegularExpressionCidenet.AZaz),
        Validators.required,
        Validators.maxLength(20)
      ]],
      primerApellido: [data ? data.primerApellido : '', [
        Validators.pattern(RegularExpressionCidenet.AZaz),
        Validators.required,
        Validators.maxLength(20)
      ]],
      segundoApellido: [data ? data.segundoApellido : '', [
        Validators.pattern(RegularExpressionCidenet.AZaz),
        Validators.required,
        Validators.maxLength(20)
      ]],
      otrosNombres: [data ? data.otrosNombres : '', [
        Validators.pattern(RegularExpressionCidenet.AZazSpace),
        Validators.required,
        Validators.maxLength(50)
      ]],
      paisEmpleo: [data ? data.paisEmpleo : null, Validators.required],
      tipoIdentificacion: [data ? data.tipoIdentificacion : null, Validators.required],
      identificacion: [data ? data.identificacion : '', [
        Validators.pattern('^[a-zA-Z0-9-]+$'),
        Validators.required,
        Validators.maxLength(20)
      ]],
      email: [ data ? data.email : '', [
        Validators.pattern(RegularExpressionCidenet.emailValid)
      ]],
      fechaIngreso: [data ? data.fechaIngreso : '', Validators.required],
      fechahoraRegistro: [data ? data.fechahoraRegistro : ''],
      fechahoraEdicion: [this.datePipe.transform(datet, 'dd-MM-yyyy HH:mm:ss')],
      area: [data ? data.area : null, Validators.required],
      estado: ['Activo']
    });

    this.fechaHora = this.editEmployeeForm.value.fechahoraEdicion;
  }

  // Getters - campos del formulario
  get primerNombre() {
    return this.editEmployeeForm.get('primerNombre');
  }

  get primerApellido() {
    return this.editEmployeeForm.get('primerApellido');
  }

  get segundoApellido() {
    return this.editEmployeeForm.get('segundoApellido');
  }

  get otrosNombres() {
    return this.editEmployeeForm.get('otrosNombres');
  }

  get paisEmpleo() {
    return this.editEmployeeForm.get('paisEmpleo');
  }

  get tipoIdentificacion() {
    return this.editEmployeeForm.get('tipoIdentificacion');
  }

  get identificacion() {
    return this.editEmployeeForm.get('identificacion');
  }

  get email() {
    return this.editEmployeeForm.get('email');
  }

  get fechaIngreso() {
    return this.editEmployeeForm.get('fechaIngreso');
  }

  get fechahoraEdicion() {
    return this.editEmployeeForm.get('fechahoraEdicion');
  }

  get area() {
    return this.editEmployeeForm.get('area');
  }

  // change controls
  changeTipoIdentificacion(e: any) {
    const valor = e.target.value;
    this.tipoIdentificacion?.setValue(+valor, {
      onlySelf: true,
    });
  }

  changePrimerNombre(e: any) {
    const valor = e.target.value;
    this.primerNombre.setValue(valor);
  }

  changePrimerApellido(e: any) {
    const valor = e.target.value;
    console.log('change email ::: ', valor);
    this.primerApellido.setValue(valor);
  }

  changeSegundoApellido(e: any) {
    const valor = e.target.value;
    console.log('change email ::: ', valor);
    this.segundoApellido.setValue(valor);
  }

  changepaisEmpleo(e: any) {
    const valor = e.target.value;
    this.paisEmpleo?.setValue(+valor);
  }

  changeArea(e: any) {
    const valor = e.target.value;
    this.area.setValue(+valor);
  }

  changeEmail(e: any) {
    const valor = e.target.value;
    console.log('change email ::: ', valor);
    this.email.setValue(valor);
  }

  changeIdentificacion(e: any) {
    const valor = e.target.value;
    console.log('change identidicacion ::: ', valor);
    this.identificacion.setValue(valor);
  }

  changeFechaIngreso(e: any) {
    const valor = e.target.value;
    console.log('change fecha in ::: ', valor);
    this.fechaIngreso.setValue(valor);
  }

  ngOnInit(): void {
    this.idEmployeEdit = this.activateRoute.snapshot.params.id;
    this.getEmployeeById(this.idEmployeEdit);
  }

  getEmployeeById(id: number) {
    this.employeeService.getEmployeeById(id).subscribe((response: Employee) => {
      this.dataEmployee = {...response};
      console.log('spread', this.dataEmployee);

      this.buildForm(this.dataEmployee);
    },
    error => {
      console.log(error);
      alert('EXCEPCIÓN CONTROLADA: ' + error.message);
    });
  }

  /**
   * Actualizar info. del empleado
   */
  async editEmployeeHandle() {
    const question = this.messagesView['message.employee.edit'];
    let emailGenerado: any;
    let identificacionRepetida: any;

    if (this.editEmployeeForm.valid) {

      if (window.confirm(question)) {

        // VALIDAR EXISTENCIA DE TIPO Y NÚMERO DE IDENTIFICACIÓN
        identificacionRepetida = await this.validationService.validarIdentificacionYtipo(this.editEmployeeForm.value);

        if (identificacionRepetida) {
          alert(this.messagesView['message.employee.warning.identification']);
          return;
        }

        // GENERAR CORREO SI Y SOLO SI, se modifica nombre + apellidos
        if (this.dataEmployee.primerNombre !== this.primerNombre.value ||
            this.dataEmployee.primerApellido !== this.primerApellido.value ||
            this.dataEmployee.segundoApellido !== this.segundoApellido.value ||
            this.dataEmployee.otrosNombres !== this.otrosNombres.value
        ) {
          // genera un email válido
          emailGenerado = await this.validationService.generateEmployeeEmail(this.editEmployeeForm.value);
          this.email.setValue(emailGenerado);
        }

        // VALIDAR EXISTENCIA DE CORREO EN FORMATO CIDENET
        emailGenerado = await this.validationService.validarEmailExistente(this.editEmployeeForm.value);

        if (emailGenerado === true) {
          // NO actualizar
          alert(this.messagesView['message.employee.warning.email']);
          return;
        }

        // actulizar la info.
        await this.employeeService.updateEmployee(this.idEmployeEdit, this.editEmployeeForm.value).subscribe(response => {
          console.log('save response: ', response);
        },
        error => {
          console.log(error);
          alert('EXCEPCIÓN CONTROLADA: ' + error.message);
        });

        this.openPopup();

      } else {
        //
      }
    } else {
      //
    }
  }

  /**
   * Permite bloquear entrada de texto en los controles
   */
    changeinput(event: any, maxlength: any) {
    if (event.target.value.length > 0) {
      if (event.target.value.length === maxlength) {
        return false;
      }
    }
  }

  // ********************************
  openPopup() {
    this.displayStyleDialog = 'block';
  }
  closePopup() {
    this.displayStyleDialog = 'none';
    this.router.navigate(['/']);
  }
}
