import { Component, OnInit, OnDestroy } from '@angular/core';
import { EmployeeService } from '../../../services/employee/employee.service';
import { Employee } from '../../../models-view/employee.model';
import { ConstantsMessageCidenet } from '../../../utils/constants/ConstantsMessageCidenet';
import { AppSettingsCidenet } from '../../../utils/AppSettingsCidenet';
import { LanguageCidenetApp } from '../../../utils/languages/LanguageCidenetApp';
import { Subject } from 'rxjs';
import { DataTablesModule } from 'angular-datatables';

@Component({
  selector: 'app-list-employee',
  templateUrl: './list-employee.component.html',
  styleUrls: ['./list-employee.component.scss']
})
export class ListEmployeeComponent implements OnInit, OnDestroy {

  listInputEmployee: any = [];
  listEmployee: any = [];
  listCountries: any = [];
  displayStyleDialog = 'none';
  actionForm = 'Eliminación';

  messagesView: any = {};

  // data table
  dtOptions: any; //DataTablesModule.Settings = {};

  // Trigger para conectar la data.
  dtTrigger: Subject<any> = new Subject<any>();

  constructor(
    private employeeService: EmployeeService
  ) {
      // cargar constantes para componentes
      this.messagesView = ConstantsMessageCidenet.getArrMessageConstants()[0];
  }

  loadEmployeesList(reinicializarDt = true) {
    let nwListEmployee: any;
    // cargar Paises
    this.employeeService.getAllCountries().subscribe((response) => {
      this.listCountries = response;
    }, error => {
      alert('EXCEPCIÓN CONTROLADA: ' + error.message);
    });

    this.employeeService.getAllEmployee().subscribe((response) => {
      this.listInputEmployee = response;

      // Normalizando - agregar KEY país a cada item del arreglo de objetos employee
      nwListEmployee =  this.listInputEmployee.map((employee: Employee) => {
        let pais: any;
        pais = this.listCountries.find(p => p.id === employee.paisEmpleo);
        return {...employee, nombrePais: pais.nombre};
      });

      this.listEmployee = nwListEmployee;

      if (reinicializarDt) {
        this.dtTrigger.next();
      }

    }, error => {
      alert('EXCEPCIÓN CONTROLADA: ' + error.message);
    });
  }

  deleteEmployee(id: number) {
    const question = this.messagesView['message.employee.delete'];

    if (window.confirm(question)) {

      this.employeeService.deleteEmployee(id).subscribe((response) => {
        console.log(response);
        this.loadEmployeesList(false);
        this.openPopup();
      });

    } else {
      //
    }
  }

  // ********************************
  openPopup() {
    this.displayStyleDialog = 'block';
  }
  closePopup() {
    this.displayStyleDialog = 'none';
  }

  ngOnInit(): void {
    // Data table conf.
    this.dtOptions = {
      pagingType: 'full_numbers',
      pageLength: +AppSettingsCidenet.LONGITUD_REGISTROS_POR_PAGE,
      lengthMenu: [10, 15, 20, 25, 30],
      processing: true,
      language: LanguageCidenetApp.spanishDatatables
    };

    this.loadEmployeesList();
  }

  ngOnDestroy(): void {
    // Do not forget to unsubscribe the event
    this.dtTrigger.unsubscribe();
  }
}
