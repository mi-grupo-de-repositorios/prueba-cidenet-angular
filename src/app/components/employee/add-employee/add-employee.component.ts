import { Component, OnInit } from '@angular/core';
import { EmployeeService } from '../../../services/employee/employee.service';
import { FormGroup, FormBuilder, Validators, EmailValidator } from '@angular/forms';
import { DatePipe } from '@angular/common';
import { RegularExpressionCidenet } from '../../../utils/RegularExpressionCidenet';
import {Router} from '@angular/router';
import { ConstantsMessageCidenet } from '../../../utils/constants/ConstantsMessageCidenet';
import { ValidationsService } from '../../../services/employee/validations.service';
import { tap, map } from 'rxjs/operators';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-add-employee',
  templateUrl: './add-employee.component.html',
  styleUrls: ['./add-employee.component.scss']
})
export class AddEmployeeComponent implements OnInit {

    // miembros base
    addEmployeeForm: FormGroup;

    listEmployee: any = [];
    listTypesIdentification: any = [];
    listCountries: any = [];
    listAreas: any = [];

    displayStyleDialog = 'none';
    actionForm = 'Inserción';
    fechaHora: string;

    messagesView: any = {};

    constructor(
      private employeeService: EmployeeService,
      private validationService: ValidationsService,
      public formBuilder: FormBuilder,
      private datePipe: DatePipe,
      private router: Router
    ) {
      this.buildForm();

      // cargar tipos de identificación
      employeeService.getAllTypesIdentification().subscribe((response) => {
        this.listTypesIdentification = response;
      },
      error => {
        console.log(error);
        alert('EXCEPCIÓN CONTROLADA: ' + error.message);
      });

      // cargar Paises
      employeeService.getAllCountries().subscribe((response) => {
        this.listCountries = response;
      },
      error => {
        console.log(error);
        alert('EXCEPCIÓN CONTROLADA: ' + error.message);
      });

      // cargar Areas
      employeeService.getAllAreas().subscribe((response) => {
        this.listAreas = response;
      },
      error => {
        console.log(error);
        alert('EXCEPCIÓN CONTROLADA: ' + error.message);
      });

      // cargar constantes para componentes
      this.messagesView = ConstantsMessageCidenet.getArrMessageConstants()[0];
    }

    private buildForm() {
      const currentDate =  new Date();

      this.addEmployeeForm = this.formBuilder.group({
        primerNombre: ['', [
          Validators.pattern(RegularExpressionCidenet.AZaz),
          Validators.required,
          Validators.maxLength(20)
        ]],
        primerApellido: ['', [
          Validators.pattern(RegularExpressionCidenet.AZaz),
          Validators.required,
          Validators.maxLength(20)
        ]],
        segundoApellido: ['', [
          Validators.pattern(RegularExpressionCidenet.AZaz),
          Validators.required,
          Validators.maxLength(20)
        ]],
        otrosNombres: ['', [
          Validators.pattern(RegularExpressionCidenet.AZazSpace),
          Validators.required,
          Validators.maxLength(50)
        ]],
        paisEmpleo: [null, Validators.required],
        tipoIdentificacion: [null, Validators.required],
        identificacion: [null, [
          Validators.pattern('^[a-zA-Z0-9-]+$'),
          Validators.required,
          Validators.maxLength(20)
        ]],
        email: ['', [
          Validators.pattern(RegularExpressionCidenet.emailValid)
        ]],
        fechaIngreso: [this.datePipe.transform(currentDate, 'dd-MM-yyyy')],
        fechahoraRegistro: [this.datePipe.transform(currentDate, 'dd-MM-yyyy HH:mm:ss')],
        fechahoraEdicion: [''],
        area: [null, Validators.required],
        estado: ['Activo'],
      });

      this.fechaHora = this.addEmployeeForm.value.fechahoraRegistro;
    }

    // Getters - campos del formulario
    get primerNombre() {
      return this.addEmployeeForm.get('primerNombre');
    }

    get primerApellido() {
      return this.addEmployeeForm.get('primerApellido');
    }

    get segundoApellido() {
      return this.addEmployeeForm.get('segundoApellido');
    }

    get otrosNombres() {
      return this.addEmployeeForm.get('otrosNombres');
    }

    get paisEmpleo() {
      return this.addEmployeeForm.get('paisEmpleo');
    }

    get tipoIdentificacion() {
      return this.addEmployeeForm.get('tipoIdentificacion');
    }

    get identificacion() {
      return this.addEmployeeForm.get('identificacion');
    }

    get email() {
      return this.addEmployeeForm.get('email');
    }

    get fechahoraRegistro() {
      return this.addEmployeeForm.get('fechahoraRegistro');
    }

    get fechaIngreso() {
      return this.addEmployeeForm.get('fechaIngreso');
    }

    get area() {
      return this.addEmployeeForm.get('area');
    }

    // change controls
    changeTipoIdentificacion(e: any) {
      const valor = e.target.value;
      this.tipoIdentificacion?.setValue(+valor, {
        onlySelf: true,
      });
    }

    changeOtrosnombres(e: any) {
      const valor = e.target.value;
      this.otrosNombres?.setValue(valor);
    }

    changepaisEmpleo(e: any) {
      const valor = e.target.value;
      this.paisEmpleo?.setValue(+valor);
    }

    changeFechaIngreso(e: any) {
      const valor = e.target.value;
      console.log('change fecha in ::: ', valor);
      this.fechaIngreso.setValue(valor);
    }

    /**
     * Permite bloquear entrada de texto en los controles
     */
    changeinput(event: any, maxlength: any) {
      if (event.target.value.length > 0) {
        if (event.target.value.length === maxlength) {
          return false;
        }
      }
    }

    changeArea(e: any) {
      const valor = e.target.value;
      this.area.setValue(+valor);
    }

    // --------------
    ngOnInit(): void {
        this.listEmployee = this.employeeService.getAllEmployee().subscribe((respose) => {
          console.log('res :: ', respose);
        },
        error => {
          console.log(error);
          alert('EXCEPCIÓN CONTROLADA: ' + error.message);
        });
    }

    /**
     * Guardar info. del empleado
     */
    async saveEmployeeHandle() {
      let emailGenerado: any;
      let identificacionRepetida: any;
      let fechaPosterior: any;
      let fechaInferiorMes: any;

      // genera un email válido
      emailGenerado = await this.validationService.generateEmployeeEmail(this.addEmployeeForm.value);

      if (this.addEmployeeForm.valid) {

        // VALIDAR EXISTENCIA DE TIPO Y NÚMERO DE IDENTIFICACIÓN
        identificacionRepetida = await this.validationService.validarIdentificacionYtipo(this.addEmployeeForm.value);

        if (identificacionRepetida) {
          alert(this.messagesView['message.employee.warning.identification']);
          return;
        }

        // VALIDAR FECHA DE INGRESO POSTERIOR
        fechaPosterior = this.validationService.validarFechaSuperior(this.fechaIngreso.value);
        if (fechaPosterior) {
          alert(this.messagesView['message.employee.warning.date.post']);
          return;
        }

        // VALIDAR FECHA DE INGRESO INFERIOR no más de 1 mes
        fechaInferiorMes = this.validationService.validarFechaInferiorHastaUnMes(this.fechaIngreso.value);
        if (fechaInferiorMes > 1) {
          alert(this.messagesView['message.employee.warning.date.inf.month']);
          return;
        }

        // asignar email
        this.email.setValue(emailGenerado);
        await this.employeeService.saveEmployee(this.addEmployeeForm.value).subscribe((response) => {
          console.log('save response: ', response);
        },
        error => {
          console.log(error);
          alert('EXCEPCIÓN CONTROLADA: ' + error.message);
        });

        this.openPopup();
      } else {
        //
      }
    }

    // ********************************
    openPopup() {
      this.displayStyleDialog = 'block';
    }
    closePopup() {
      this.displayStyleDialog = 'none';
      this.router.navigate(['/']);
    }
}
