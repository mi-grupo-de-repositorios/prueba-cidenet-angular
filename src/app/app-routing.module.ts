import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AddEmployeeComponent } from './components/employee/add-employee/add-employee.component';
import { ListEmployeeComponent } from './components/employee/list-employee/list-employee.component';
import { EditEmployeeComponent } from './components/employee/edit-employee/edit-employee.component';

// COMPONENTES CREADOS

const routes: Routes = [
    {
        path: 'add',
        component: AddEmployeeComponent
    },
    {
        path: 'edit/:id',
        component: EditEmployeeComponent
    },
    {
        path: 'home',
        component: ListEmployeeComponent
    },
    {
      path: '',
      component: ListEmployeeComponent
  },
];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})
export class AppRoutingModule { }
