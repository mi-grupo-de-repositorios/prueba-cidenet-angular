export interface Employee {
    id: number;
    primerNombre: string;
    segundoNombre: string;
    primerApellido: string;
    segundoApellido: string;
    otrosNombres: string;
    paisEmpleo: number;
    tipoIdentificacion: number;
    identificacion: string;
    email: string;
    fechaIngreso: Date;
    area: number;
    estado: string;
    fechahoraRegistro: Date;
    fechahoraEdicion: Date;
}
