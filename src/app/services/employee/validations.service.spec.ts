import { TestBed } from '@angular/core/testing';
import { HttpClient } from '@angular/common/http';
import {HttpClientModule} from '@angular/common/http';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { ValidationsService } from './validations.service';
import { AppSettingsCidenet } from '../../utils/AppSettingsCidenet';
import { Employee } from '../../models-view/employee.model';

describe('ValidationsService', () => {
  let service: ValidationsService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, HttpClientModule]
    });
    service = TestBed.inject(ValidationsService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('Validando retorno de dominio email para el país COLOMBIA', () => {
    expect(service.getDomainEmployee(1)).toBe(AppSettingsCidenet.DOMINIO_EMAIL_CO);
  });

  it('Validando retorno de dominio email para el país EEUU', () => {
    expect(service.getDomainEmployee(2)).toBe(AppSettingsCidenet.DOMINIO_EMAIL_USA);
  });

  it('Validando EMAIL con menos de 300 caracteres', (() => {
    const email = 'jaime.diaz.jamygaona@cidenet.com.co';
    const testAsync  = async () => {
      expect(await service.evaluarLongitudEmailYretornar(email)).toBe(email);
    };
  }));

  it('Validando EMAIL con más de 300 caracteres',  () => {
    // tslint:disable-next-line:max-line-length
    const email = 'jaimeivandiazgaona.probandoemailconmasde300caracteres.segundaparteconmasde300caracteres@dominiodepruebaconmasde300caracteresdominiodepruebaconmasde300caracteresdominiodepruebaconmasde300caracteresdominiodepruebaconmasde300caracteresdominiodepruebaconmasde300caracteresdominiodepruebaconmasde300caracter.com';
    // tslint:disable-next-line:max-line-length
    const emailEsperado = 'jaimeivandiazgao.probandoemailconmasde300caracter.segundaparteconmasde300caracter@dominiodepruebaconmasde300caracteresdominiodepruebaconmasde300caracteresdominiodepruebaconmasde300caracteresdominiodepruebaconmasde300caracteresdominiodepruebaconmasde300caracteresdominiodepruebaconmasde300caracter.com';
    const testAsync = async () => {
      expect(await service.evaluarLongitudEmailYretornar(email)).toBe(emailEsperado);
    };
  });

  it('Validando generación de un email por partes',  () => {
    const employee: any = {};
    employee.id = 1;
    employee.primerNombre = 'testinga';
    employee.primerApellido = 'apellidoa';
    employee.segundoApellido = 'apellidob';
    employee.otrosNombres = 'otros';
    employee.paisEmpleo = 1;

    const emailEsperado = 'testinga.apellidoa.apellidob@cidenet.com.co';
    const testAsync = async () => {
      expect(await service.generateEmployeeEmail(employee)).toBe(emailEsperado);
    };
  });

  it('Validando un email que NO existe en la base de datos',  () => {
    const employee: any = {};
    employee.id = 1;
    employee.primerNombre = 'testinga';
    employee.primerApellido = 'apellidoa';
    employee.segundoApellido = 'apellidob';
    employee.otrosNombres = 'otros';
    employee.paisEmpleo = 1;
    employee.email = 'email.inexistente.otrosnombres@cidenet.com.co';

    const testAsync = async () => {
      expect(await service.validarEmailExistente(employee)).toBe(false);
    };
  });
});
