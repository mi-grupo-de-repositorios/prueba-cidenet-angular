import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { AppSettingsCidenet } from '../../utils/AppSettingsCidenet';
import { Employee } from '../../models-view/employee.model';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class ValidationsService {

  constructor(
    private http: HttpClient
  ) {}

  getEmployeeByEmail(email: string): Observable<any> {
    // tslint:disable-next-line:max-line-length
    return this.http.get<any[]>(`${AppSettingsCidenet.PATH_API_BACKENDDUMMY}/${AppSettingsCidenet.EMPLOYEE_ENDPOINT}?email=${email}`).pipe(
      map((res) => {
        return res;
      })
    );
  }

  // validar numero identificación
  validateIdentificationNumberAndType(identification: string, typeIdentification: number): Observable<any> {
    // tslint:disable-next-line:max-line-length
    return this.http.get<any[]>(`${AppSettingsCidenet.PATH_API_BACKENDDUMMY}/${AppSettingsCidenet.EMPLOYEE_ENDPOINT}?identificacion=${identification}&tipoIdentificacion=${typeIdentification}`).pipe(
      map((res) => {
        return res;
      })
    );
  }

  async validarIdentificacionYtipo(employee: Employee) {
    let existeStatus: any;

    // tslint:disable-next-line:max-line-length
    existeStatus = await this.validateIdentificationNumberAndType(employee.identificacion, employee.tipoIdentificacion).toPromise().then((res: any) => {
      let validacion: any;

      if (res.length > 0) {
        if (res[0].identificacion === employee.identificacion &&
            res[0].tipoIdentificacion === employee.tipoIdentificacion &&
            res[0].id !== employee.id
        ) {
          validacion = true;
        } else {
          validacion = false;
        }
      } else {
        validacion = false;
      }
      return validacion;
    });
    return existeStatus;
  }

  async validarEmailExistente(employee: Employee) {
    let existeStatus: any;

    existeStatus = await this.getEmployeeByEmail(employee.email).toPromise().then((res: any) => {
      let responseStatus: any;

      console.log('objeto del servicio: ', employee);
      console.log('objeto del servidor: ', res);

      if (res.length > 0) {
        if (res[0].email === employee.email && (+employee.id !== +res[0].id)) {
          responseStatus = true;
        } else {
          responseStatus = false;
        }
      } else {
        responseStatus = false;
      }
      return responseStatus;
    });

    return existeStatus;
  }

  /**
   * Genera un email y valida que no exista previamente en la base de datos
   */
  async generateEmployeeEmail(employee: Employee) {

    let name: string;
    let name2: string;
    let otros: string;
    let emailGenerado: any;

    if (employee.primerNombre) {
      name = employee.primerNombre;
    } else {
      name = '';
    }

    if (employee.primerApellido) {
      name2 = employee.primerApellido;
    } else {
      name2 = '';
    }

    // quitando espacios de otros nombres
    if (employee.otrosNombres) {
      otros = employee.otrosNombres;
      otros = otros.replace(/ /g, '');
    } else {
      otros = '';
    }

    // GENERACIÓN SEGMENTADA
    emailGenerado = await this.getUnionPartesEmailStatus(name, name2, otros, this.getDomainEmployee(employee.paisEmpleo));

    // EVALUAR LONGITUD
    emailGenerado = await this.evaluarLongitudEmailYretornar(emailGenerado);

    return emailGenerado;
  }

  /**
   * Evalúa longitud del email, si es más de 300 caracteres
   * se procesa las partes del correo para ir quitando caracteres
   * por cada iteración.
   */
  async evaluarLongitudEmailYretornar(email: string) {
    let emailTransform: any;
    let emailAux: any;
    let parte1: any;
    let parte1Aux: any;
    let parteDominio: any;
    let parte2Aux: any;
    let parte3Aux: any;

    emailTransform = email;

    if (email.length > AppSettingsCidenet.LONGITUD_EMAIL_MAX) {
      emailAux = email.split('@');

      if (emailAux.length > 0) {
        parte1 = emailAux[0];
        parteDominio = emailAux[1];
        parte1 = parte1.split('.');

        // ** Descomponer antes del @ las posibles 3 partes separadas por '.'

        // primer nombre
        if (parte1.length > 0) {
          parte1Aux = parte1[0];
        }
        // primer apellido
        if (parte1.length >= 1) {
          parte2Aux = parte1[1];
        } else { parte2Aux = ''; }
        // seg apellido
        if (parte1.length >= 2) {
          parte3Aux = parte1[2];
        } else { parte3Aux = ''; }

        let counter = 1;

        do {
          emailTransform = '';
          parte1Aux = parte1Aux.slice(0, -1 * counter);
          parte2Aux = parte1Aux.slice(0, -1 * counter);
          parte3Aux = parte1Aux.slice(0, -1 * counter);

          emailTransform = `${parte1Aux}.${parte2Aux}.${parte3Aux}@${parteDominio}`;

          counter ++;
        }
        while (emailTransform.length <= AppSettingsCidenet.LONGITUD_EMAIL_MAX);
      }
    }

    console.log('evaluar email ', emailTransform);

    return emailTransform;
  }

  getDomainEmployee(pais: number) {
    return (pais === 1 ? AppSettingsCidenet.DOMINIO_EMAIL_CO : AppSettingsCidenet.DOMINIO_EMAIL_USA);
  }

  async getUnionPartesEmailStatus(
    primerNombre: string,
    primerApellido: string,
    otrosNombres: string,
    dominio: string
  ) {
    let emailCompuesto = '';

    emailCompuesto += (primerNombre !== '' ? primerNombre + '.' : '');
    emailCompuesto += (primerApellido !== '' ? primerApellido + '.' : '');
    emailCompuesto += (otrosNombres !== '' ? otrosNombres : '');

    // validar existencia
    let emailConNumeracion = '';
    let contadorEmails = 0;
    let existeStatus: any;

    do {

      if (contadorEmails > 0) {

        emailConNumeracion = emailCompuesto + '.' + contadorEmails + '@' + dominio;
        existeStatus = await this.getEmployeeByEmail(emailConNumeracion).toPromise().then(res => {
          return res.length > 0 ? true : false;
        });

      } else {

        emailConNumeracion = emailCompuesto + '@' + dominio;
        existeStatus = await this.getEmployeeByEmail(emailConNumeracion).toPromise().then(res => {
          return res.length > 0 ? true : false;
        });

      }

      contadorEmails ++;
    }
    while (existeStatus === true);

    return emailConNumeracion;
  }

  // Validando fechas
  validarFechaSuperior(fecha: Date) {
    const hoy = new Date();
    fecha = new Date(fecha);

    console.log('valida fechas: 1: hoy: ', hoy, ' ingreso: ', fecha);
    hoy.setHours(0, 0, 0, 0);
    fecha.setHours(0, 0, 0, 0);

    if (fecha > hoy) {
      return true;
    } else {
      return false;
    }
  }

  validarFechaInferiorHastaUnMes(fecha: Date) {
    const hoy = new Date();
    fecha = new Date(fecha);
    hoy.setHours(0, 0, 0, 0);
    fecha.setHours(0, 0, 0, 0);

    return this.monthDiff(fecha, hoy);
  }

  monthDiff(dateFrom, dateTo) {
    return dateTo.getMonth() - dateFrom.getMonth() +
      (12 * (dateTo.getFullYear() - dateFrom.getFullYear()));
  }
}
