import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { AppSettingsCidenet } from '../../utils/AppSettingsCidenet';
import { Employee } from '../../models-view/employee.model';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class EmployeeService {

    constructor(private http: HttpClient) {}

    /**
     * Devuelve una lista de Empleados
     */
    getAllEmployee(): Observable<any> {
      try {
        return this.http.get(`${AppSettingsCidenet.PATH_API_BACKENDDUMMY}/${AppSettingsCidenet.EMPLOYEE_ENDPOINT}`);
      } catch (error) {
        // tslint:disable-next-line:no-string-throw
        throw new Error('Error al conectar con la base de datos');
      }
    }

    getAllTypesIdentification() {
      return this.http.get(`${AppSettingsCidenet.PATH_API_BACKENDDUMMY}/${AppSettingsCidenet.TYPE_IDENTIFICATION_ENDPOINT}`);
    }

    getAllCountries() {
      try {
        return this.http.get(`${AppSettingsCidenet.PATH_API_BACKENDDUMMY}/${AppSettingsCidenet.COUNTRY_ENDPOINT}`);
      } catch (error) {
        // tslint:disable-next-line:no-string-throw
        throw new Error('Error al conectar con la base de datos');
      }
    }

    getAllAreas() {
      try {
        return this.http.get(`${AppSettingsCidenet.PATH_API_BACKENDDUMMY}/${AppSettingsCidenet.AREA_ENDPOINT}`);
      } catch (error) {
        throw new Error('Error al conectar con la base de datos');
      }
    }

    getEmployeeById(id: number) {
      try {
        return this.http.get(`${AppSettingsCidenet.PATH_API_BACKENDDUMMY}/${AppSettingsCidenet.EMPLOYEE_ENDPOINT}/${id}`);
      } catch (error) {
        throw new Error('Error al conectar con la base de datos');
      }
    }

    // POST ACTIONS
    saveEmployee(employee: Employee) {
      try {
        return this.http.post(`${AppSettingsCidenet.PATH_API_BACKENDDUMMY}/${AppSettingsCidenet.EMPLOYEE_ENDPOINT}`, employee);
      } catch (error) {
        throw new Error('Error al conectar con la base de datos');
      }
    }

    deleteEmployee(id: number) {
      try {
        return this.http.delete(`${AppSettingsCidenet.PATH_API_BACKENDDUMMY}/${AppSettingsCidenet.EMPLOYEE_ENDPOINT}/${id}`);
      } catch (error) {
        throw new Error('Error al conectar con la base de datos');
      }
    }

    // PUT - PATCH ACTIONS
    updateEmployee(id: number, employee: Employee) {
      try {
        return this.http.put(`${AppSettingsCidenet.PATH_API_BACKENDDUMMY}/${AppSettingsCidenet.EMPLOYEE_ENDPOINT}/${id}`, employee);
      } catch (error) {
        throw new Error('Error al conectar con la base de datos');
      }
    }
}
