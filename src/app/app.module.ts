import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NgbModule, NgbDatepickerModule, NgbTimepickerModule } from '@ng-bootstrap/ng-bootstrap';
import { AddEmployeeComponent } from './components/employee/add-employee/add-employee.component';
import { EmployeeService } from './services/employee/employee.service';
import { ListEmployeeComponent } from './components/employee/list-employee/list-employee.component';

import { ReactiveFormsModule } from '@angular/forms';
import { DatePipe } from '@angular/common';
import { EditEmployeeComponent } from './components/employee/edit-employee/edit-employee.component';
import { ValidationsService } from './services/employee/validations.service';
import { DataTablesModule } from 'angular-datatables';
import { NavbarComponent } from './components/uielements/navbar/navbar.component';
import { FooterComponent } from './components/uielements/footer/footer.component';

@NgModule({
  declarations: [
    AppComponent,
    AddEmployeeComponent,
    ListEmployeeComponent,
    EditEmployeeComponent,
    NavbarComponent,
    FooterComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    NgbModule,
    HttpClientModule,
    ReactiveFormsModule,
    NgbDatepickerModule,
    NgbTimepickerModule,
    DataTablesModule
  ],
  providers: [
    EmployeeService,
    ValidationsService,
    DatePipe
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
