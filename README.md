# PruebaCidenet

## Instalación de paquetes de NODE relativos al prouecto
Como paso inicial, deberá descargar el proyecto desde Gitlab a su entorno local, posterior a ello deberá instalar los paquetes a través del comando:
`npm install`
## Correr el servidor BACKEND DUMMY
Este proyecto contiene un fichero en formato JSON el cual servirá como BACKEND DUMMY.
Basta entonces con seguir los siguientes pasos para correr el servidor backend:

1. Deberá instalar primero el paquete json-serve con el siguiente comando:
 `npm install --save json-server`
2. Una vez instalado el paquete anterior, debe situarse en el directorio que contiene el fichero JSON dentro del proyecto, con el nombre: 'backend-dummy', el cual tiene el nombre de:  db.json
![captura de pantalla](https://jaimediaz.dev/imagenes_externas/prueba_cidenet/captura_folder_dbjson.png)

3. Una vez situados en el directorio donde se encuentra el fichero db.json, se procede a abrir la terminal y ejecutar el comando: `json-server --watch db.json` y con ello quedará corriendo el backend en el puerto 3000 por defecto:

![captura de pantalla](https://jaimediaz.dev/imagenes_externas/prueba_cidenet/dbjson_corriendo.png)

4. Una vez corriendo el comando, por consola deberá visualizar la url y el puerto a través del cual podrá hacer uso de su entorno local para interactuar con los recursos, basta entonces con abrir dicha URL en el navegador:

![captura de pantalla](https://jaimediaz.dev/imagenes_externas/prueba_cidenet/recursos_page.png)
## Desplegar la aplicación en un servidor local

Para correr el proyecto basta con situarse en el directorio desde donde se descargó el proyecto y ejecutar el siguiente comando: `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Correr las pruebas unitarias implementadas para testear métodos en el servicio de Validaciones - CIDENET

Correr el comando  `ng test --code-coverage`

- Deberá ver por consola la siguiente salida:

![captura de pantalla](https://jaimediaz.dev/imagenes_externas/prueba_cidenet/consola_pruebas_unitarias.png)

- En el navegador podrá ver entonces la siguiente salida que corresponde a mostrada en la consola:

![captura de pantalla](https://jaimediaz.dev/imagenes_externas/prueba_cidenet/navegador_pruebas_unitarias.png)

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Correr las pruebas unitarias implementadas para testear métodos en el servicio de Validaciones - CIDENET

Correr el comando  `ng test`


